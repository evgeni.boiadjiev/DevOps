# `Kubernetes Services explained`
# ClusterIP vs NodePort vs LoadBalancer vs Headless Service

<p>
<a href="https://www.youtube.com/watch?v=T4Z7visMM4E"><img src="../youtube.png" height=25></a>
</p>

#
###  there are 3 types services / adn 1 not defined  `(Headless)`

1. **ClusterIP**
2. **NodePort**
3. **LoadBalancer**

###
```
kubectl get svc --all-namespaces 
kubectl get svc -n  < namespaces-name >
```
### 1. ClusterIP              `(is internal Service)`
- defatul type / no type specified
- ingres finds services by name (servicename:) as local dns resolution
- services use selector to define deployment pods members (selector:)
- if pods has multiple ports open  /  how to  deside   which port to forword request to ? by (targetport:)
- `when we create service, kubernetes create end point object with same name as service.`
- `kubectl get endpoints --all-namespaces`
```diff            
 
+      (INGRESS CONTROLER)                            <------ validates incoming traffic information based on entry rules.
               /\
               ||
               ||
               \/                                     //===> pod1
 BROWSER ==> INGRESS ==> SERVICE:port (ClusterIp)  ====            
                                                      \\===> pod2
```
### Headless  ClusterIP: None
1. It is not randomly selected, it use to directly communicate with stateful application (DB, mysql)
2. ClusterIP is set (ClusterIP: Non)
```
            //==<< pod1
           || 
            \\==>> pod2
```
### 2. NodePort 
1. (**30000 - 32767**) it use predifine range            
2. it is not recommended because it is not secure

```
            //==> Node1:30000
BROWSER ===||
            \\==> Node2:30000 
```
### 3. LoadBalancer
1. It become accesible externally trought cloud provider LoadBalancer

```
                                                                   //===> pod1
BROWSER ==> LoadBalancer ==> SERVICE ( NodePort / ClusterIp )  ====            
                                                                   \\===> pod2

-- loadBalancer Service is extension of NodePort services
-- NodePort Service is extension of ClusterIP services
```
###
