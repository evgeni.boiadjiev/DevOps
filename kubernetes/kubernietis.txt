

                               https://www.youtube.com/watch?v=X48VuDVv0do&t=933s
               
                         KUBERNETIS DEFINITION ## Open source container orchestration tool

                                #################################################
                                #             BASIC CORE COMPONENTS             #  5:20
                                #################################################

pods                                                       # ABSTARCTION OVER CONTAINER / smallest unit of k8s / EACH POD GETS OWN IP
service                                                    # IT IS PERMANETN IP / use for comunication b/w pods 
                                                           internal 
                                                           external

Ingress                                                    # route traffic into cluster
ConfigMap                                                  # external configuration of applications  (for example DB_URL )

 
Secret                                                     # external config use to store secret data ( user/pass ) use base64 encoded
                                                           DB_USER
                                                           DB_PASS

Volumes                                                    # DATA PERSISTENT / STORE DB /local or remote 
Deployments                                                # MANY REPLICATED PODS /  Deployment mange pods 
StatefulSet                                                # DB REPLICATION CAN'T DO WITH Deployments

                                #################################################
                                #               CLUSTER STRUCTURE               #  22:29
                                #################################################
        
                               THERE ARE TWO TYPE OF NODES  /  master & worker nodes

WORKER nodes MANDATORY PROCESSES

1. conteriner run time machine                             # on every node ( Docker / CRI-O / Containerd) 
2. kublet                                                  # config  and comunication b/w nods 
3. kube-proxy                                              # take care of the communication  b/w pods

MASTER nodes MANDATORY PROCESSES

1 Api Server                                               # cluster gateway / use to Deploy new application / API - kubectl - KubernetesDashboard
2 Schedule                                                 # DESIDE WHERE TO PUT THE POD                     / based on available resources
3 Controller manger                                        # DETECT CLUSTER STATE CANGES                     / REQUEST Schedule TO RECREATE pods 
4 etcd                                                     # Cluster Brain                                   / store all about pos and changes 


                                #################################################
                                #                   minikube                    #  34:47
                                #################################################

                                #################################################
                                ##            Basic kubectl commands           ##  44:52
                                #################################################

kubectl get nodes                                             # GET STATUS OF CLUSTER NODES
kubectl get pods                                              # GET STATUS OF PODS
kubectl get services                                          # GET SERVICE / STATIC IP
kubectl get deployments                                       # GET STATUS OF Deployments / you shouldn't work with pod, always work with Deployment


kubectl create deployment my-server-name --image=nginx        # CREATE NEW DEPLOYMENT   // get last image form  https://hub.docker.com/_/nginx
kubectl edit   deployment my-server-name                      # Use vim hot key / to save cahnges  put :wq or :x!
kubectl delete deployment my-server-name                      # DELETE

kubectl get deployments                                       # GET DEPLOYMMENT STAATUS   0/1 NOT WORK / 1/1  IS OK
kubectl get pods                                              # same shit
kubectl get replicaset                                        # SHOWS HOW MANY REPLACES THERE ARE / never use directly config by Deployment 
kubectl get all


                                ##################################################
                                #                    DEBUG PODS                  #
                                ##################################################
kubectl get pod
kubectl logs my-server-name-6979446c44-lsqgp                   # SHOW POD LOGS 
kubectl describe pod my-server-name-6979446c44-lsqgp           # GET ALL POD INFORMATION
kubectl exec -it my-server-name-6979446c44-lsqgp -- bin/bash   # ENTER INTO  POD
hostname -i                                                    # GET IP INTO POD

                                ##################################################
                                #           create or edit config file           #
                                ##################################################

kubectl apply -f nginx-deployment.yaml                         # always could be edit and apply again with the same command
kubectl delete -f nginx-deployment.yaml                        # delete deployment 

kubectl get deployments  nginx-deployment -o yaml  > nginx-deployment.yaml
                                   
                                ##################################################
                                # configurationson components in the K8S cluster #  1:02
                                ################################################## 

                                     kubernetis config files has 3 prats

1. metadata                                                    # contains mandatory information  
2. Specification                                               # contains specific to the kind information  
3. Status                                                      # generated by kubernetes  ( Desired =/= Actual ) / if does not match kubernetes knows shoud be fixed.

## EXAMPLE FILE  EXAMPLE_1/3parts.yaml
## http://www.yamllint.com/   yaml validator

Connectint different components using  Labels and Seletcors

## EXAMPLE FILE  EXAMPLE_2/nginx-deployment.yaml
## EXAMPLE FILE  EXAMPLE_2/nginx-service.yaml

kubectl get pod -o wide 
kubectl get service 
kubectl describe service < service-name >

kubectl get deployments  nginx-deployment -o yaml  > nginx-deployment.yaml


                                ################################################# 
                                #  Demo Project: MongoDB and MongoExpress       #  1:16:16
                                #################################################
# https://hub.docker.com/_/mongo
# EXAMPLE FILE  EXAMPLE_3/
# it works in minikube 
# kubectl apply -f ../EXAMPLE_3/
# minikube service mongo-express-service
#

# creation order

1. Secret       # kind: Secret 
2. Deploymenta  # kind: Deployment
3. service      # 
4. ConfigMapa   # content DB URL 

# CREATE USER AND PASS 
$ echo -n user | base64
dXNlcg==
$ echo -n password | base64
cGFzc3dvcmQ=

                                ##################################################
                                #              Kubernetes Namespaces             #  1:46
                                ##################################################

      virtual cluster inside kubernetes cluster / it groups resources to work together / it could be used to organize user access 

kubectl get namespace
kubectl create namespace my-namespace
kubectl api-resources --namespaced=false                       # show components which aren't bound with namespaces
kubectl api-resources --namespaced=true                        # show components which are bound with namespaces
kubectl create namespace my-namespace                          # firs create namespace
kubectl apply -f nginx-deployment.yaml --namespace=my-namespace   ## then use

1. kube-system                                                 #  Do not creat or modify / use fro system processes
2. kube-public                                                 #  Content public acesible data / Do not need autentication 
3. kube-node-lease                                             #  keep information about herardbeats of nodes 
4. default                                                     #  Use to create resources at the beginning if you have not create new namespace
                             
                                ##################################################
                                ##            K8s Ingress explained             ##  2:00
                                ##################################################
                                           
## EXTERNAL VS INGRESS
## schem ingress >> service (internal) >> pod 
## USE FILES  ingress.yaml  internal-service.yaml

ingress / use (Ingress Controler Pod) / Ingress  has attribute called default backend

!!!! https !!!!
                               
                                #################################################
                                ##           Helm - Package Manager            ##  2:24:17  
                                #################################################

## Helm Cart 

                                ##################################################
                                ##     Persisting Data in K8s with Volumes      ##  2:38:07 
                                ##################################################

##  Volumes are not namespaces and do not use namespace. They are available for all namespaces

1. Persistent Volume                       # a  cluster resource / it uses physical storage / it could be  nfs , google cloud or local 
2. Persistent Volume Claim                 # It has access type  / wr , ro  / pvc is linked to pv  // pods use pvc - pvc use pv 
3. Storage Class                           # Storage Class create PV (Persistent Volume) dynamically    / it create by kubernetis.io - internal provisioner or external profisioner / PVC use  Storage Class 

                                         
                                #################################################
                                #   Deploying Stateful Apps with StatefulSet    #
                                #################################################   2:58:38 

StatefulSet                                # DB REPLICATION CAN'T DO WITH Deployments

when pod restart:               
                 
                   1. IP address changes 
                   2. Name ened endpoint stay same


                                #################################################
                                #             K8s Services explained            # 3:13:43
                                #################################################    


Service                                     #  It is stable Static IP  
    
       1. Stable IP address 
       2. locabalancing 


ClusterIP                                   # Defatul type / No type specified 
 
               ingress  >>> Serice:port 


Service types

1. ClusterIP 
