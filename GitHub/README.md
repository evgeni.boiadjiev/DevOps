# Github how to

### Show Git version
```
git --version
```
### Credential config
```
git config --global user.name "Evgeni Boiadjiev"
git config --global user.email "evgeni.boiadjiev@gmail.com"
```
## Check Credentials
```
git config --global --list
git config --global user.name 
git config --global user.email
```

## Create a new project 
```
git config --global user.name "Evgeni Boiadjiev"
git config --global user.email "evgeni.boiadjiev@gmail.com"
mkdir Project1
cd Project1 
git init
git add .
touch FirsTes.txt
git status
git remote add origin https://gitlab.com/evgeni.boiadjiev/Project1.git
git add . 
```
## OR
```
git add FirsTes.txt
git commit -m "Version 1.1"
git push -u origin --all
```
## OR
```
git push -u "https://gitlab.com/evgeni.boiadjiev/Project1.git" master
```
##

## Add a new directory to the project
```
mkdir Project1
touch Project1/file.txt
git commit -m "Version 1.5"
git push -u origin --all
```
## OR
```
git push -u "https://gitlab.com/evgeni.boiadjiev/Project1.git" master
```

## Check commit
```
git log
git log --all --decorate --oneline --graph
```
## Change branch  
```
git branch  test                      ---  create new branch 
git checkout master
git branch 
git diff master NewMaster 
git branch -d NewBranch  
git merge "from source branch"
```
## Remove File 
```
git rm --cached FileName.txt
git status 
```
### GIT clone pull push
```
git clone  https://gitlab.com/evgeni.boiadjiev/Project1.git
git pull  https://gitlab.com/evgeni.boiadjiev/Project1.git

git remote 
git remote -v
git pull origin master
git push origin master 
```
