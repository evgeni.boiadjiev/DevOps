
## Steps
### 1 — Go back to your base directory
```
$ cd ~/
```
### 2 — Create the working directory
```
$ mkdir docker-nginx-demo
```
### 3 — Go into that directory
```
$ cd docker-nginx-demo
```
### 4 — Create the Docker & Docker Compose configuration
```
$ touch docker-compose.yaml
```
### 5 — Create the directory that will hold your HTML files
```
$ mkdir src
```
### 6 — Write something for your first page
```
$ echo "Hello world!" > src/index.html
```
### 7 — Edit your Docker Compose configuration file
```
$ vim docker-compose.yaml

version: "3"

services:
    client:
        image: nginx
        ports:
            - 8000:80
        volumes:
            - ./src:/usr/share/nginx/html
```
### 8 — Start the server
```
$ docker-compose up --detach
```
### 9 — Open your page
```
$ chromium http://localhost:8000
```
## 10 — Enjoy!
