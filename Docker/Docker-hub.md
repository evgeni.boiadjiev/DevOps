## This is simplest way to create and upload own images on docker hub
______
1. *You need to register first* https://hub.docker.com
2. *Next you could create and upload images*
______
### Show local images
```
# docker images
```
###  login / logout to  hub.docker.com
```
# docker login
# docker logout
```

### Show local images
```
# docker images

REPOSITORY   TAG       IMAGE ID       CREATED      SIZE
nginx        latest    c316d5a335a5   8 days ago   142MB
```
### Use local images to create own
```
# docker tag nginx eboiadjiev/my-web1.0

-- nginx              - images base on it     
-- eboiadjiev         - hub.docker.com login name
-- my-web1            - repositorie name
```
### Show local images
```diff
# docker images 
  REPOSITORY             TAG       IMAGE ID       CREATED      SIZE
+ eboiadjiev/my-web1.0   latest    c316d5a335a5   8 days ago   142MB
  nginx                  latest    c316d5a335a5   8 days ago   142MB
```
### Upload my new image to https://hub.docker.com/repositories
```
# docker push eboiadjiev/my-web1.0
```
### Download my new image to https://hub.docker.com/repositories
```
# docker pull eboiadjiev/my-web1.0
```
