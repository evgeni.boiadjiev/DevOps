
## Steps
### 1 — Go install directory
```
$cd PDNS-RECURSOR/

```
### 2 — Install with docker-compose.yaml
```
$ docker-compose up --detach
or
$ docker-compose -f docker-compose.yaml up -d
```
### 3 — Edit config  (option only if necessary)
```
$ sudo vi /var/lib/docker/volumes/pdns-recursor_pDNS/_data/recursor.conf
$ docker container stop PDNS-RECURSOR
$ docker container start PDNS-RECURSOR
```
### 4 — Ceheck container
```diff
  docker container ls
- Output should be like this
  CONTAINER ID   IMAGE                       COMMAND                  CREATED          STATUS         PORTS                               NAMES
+ 2e545d2b3e3f   rockylinux/rockylinux:8.4   "/usr/sbin/pdns_recu…"   15 minutes ago   Up 3 minutes   0.0.0.0:53->53/udp, :::53->53/udp   PDNS-RECURSOR
```
### 5 — Check remotely
```
$ dig @host_ip domain.com
```
### 6 — Enjoy!

