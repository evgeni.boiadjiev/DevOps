#!/bin/bash

wget https://www.cacti.net/downloads/spine/cacti-spine-latest.tar.gz
tar xfz cacti-spine-latest.tar.gz
cd cacti-spine-*/
./bootstrap
./configure
 make
 make install
 chown root:root /usr/local/spine/bin/spine
 chmod +s /usr/local/spine/bin/spine
