#!/usr/bin/bash  

DIR=websites

echo "Did you edit ".env"  yes/no"
echo 
while :
do
  read INPUT_STRING
  case $INPUT_STRING in
      yes)      
		echo "OK"
                break
		;;
       no)
		echo "You need to edit .env please!" 
                exit 0
		;;
	*)
		echo "Sorry, I don't understand"
                echo "Put yes/no" 
		;;
  esac
done


. .env
 
mkdir ../$DIR/$NAME
cp .env ../$DIR/$NAME


echo "WWW              : 1"
echo "DB and WWW       : 2"
echo "WWW, DB, PHPmyADM: 3" 

while :
do
  read SERVICES
  case $SERVICES in
        1)
                docker-compose  -f WWW.yml config > ../$DIR/$NAME/docker-compose.yml 
                break
                ;;
        2)
                docker-compose -f WWW.yml -f DB.yml config > ../$DIR/$NAME/docker-compose.yml
                break
                ;;
        3)
                echo "this option is not implemented yet"
                exit 0
                ;;

        *)
                echo "Sorry, I don't understand"
                echo "Put 1,2 or 3"
                ;;
  esac
done


FILE="../$DIR/$NAME/docker-compose.yml"

if [ -f "$FILE" ]; then
    sed -i "s/my/$NAME\_/g"  ../$DIR/$NAME/docker-compose.yml
    sed -i 's/\$\$/\$/g'  ../websites/$NAME/docker-compose.yml
else 
    echo "$FILE does not exist."
fi

