## https://hub.docker.com/ 
```
docker container inspect NAME
```
### SHOW ALL CONTAINERS
```
docker ps                           
docker ps -a 
```
### LIST IMAGES
```
docker images                      
docker image rm name
```
### PULL IMAGES / PULL EXACT IMAGE
```
docker pull debian                  
docker pull debian:jessie           
```
### DOWNLOAD,INSTALL,RUN CREATE CONTAINER
```
docker run postgres:10.10        
```
### IN BACKGROUND
``` 
docker run -d postgres:10.10      
```
### -pHOST:CONTAINER 
```
docker run -p8080:80 
```              
docker run --network My-New-br 

### start/stop container
```
docker start/stop  name             
```
### ON BOOT
```
docker update --restart unless-stopped NAME              
docker update --restart unless-stopped $(docker ps -q)   
```

docker run -d -p:6001:6379 --name redis-older redis

### READ LOGS
```
docker logs 82442da607fd         
```
### LOGIN INTERACTIVE SHELL
```      
docker exec -it redis-older /bin/bash  ## LOGIN INTERACTIVE SHELL             
# env
# hostname -i  

```
### SHOW NETWORK
```
docker network ls                      
docker network create My-New-br
docker network rm     My-New-br
```
### INSTALL WITH DOCKER COMPOSE
```
docker-compose -f file.yml up         
``` 
### REMOVE CONTAINER
```
docker rm f10d49a0a7e9                 
docker rm name                   
```
### 3 Types
```
docker -v /phisical:/var/lib/mysql/daata  -- HOST VOLUMES          -- docker info | grep -i "Docker Root Dir"
docker -v /var/lib/mysql/data             -- ANONIMOS VOLUMES
docker -v  name:/var/lib/mysql/data       -- NAMED VOLUMES 
```
### docker run,copy,cmd 

```
docker run -e TZ=Europe/Sofia -d --name CACTI-NEW  -p 80  --network  My-New-br quantumobject/docker-cacti 
```
#Build an image from a Dockerfile  ( -t = tag)
```
docker build -t my-app:1.0 .
```

```
docker run --name CACTI-MYSQL  -v CACTI-MYSQL:/var/lib/mysql  -e MYSQL_ROOT_PASSWORD=PASSWORD --net CACTI-BR -d mysql
docker run --name PHP-Myadmin  -v PHP-Myadmin:/etc/phpmyadmin --link CACTI-MYSQL:db -p 8080:80 --net CACTI-BR -d phpmyadmin
docker run --name WEB-NGINX    -v ./cacti-1.2.19:/usr/share/nginx/html              -p 8008:80 --net CACTI-BR -d nginx
docker run --name APACHE-WEB   -v ./cacti-1.2.19:/var/www/html/         -p 8008:80 --net CACTI-BR -d bitnami/apache:latest
docker run --name APACHE-WEB   -v "$(pwd)"/src/test:/var/www/html       -p 8787:80 --net CACTI-BR -d  apache-php-fpm:latest
```

```
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi -f $(docker images -q) 
docker volume rm $(docker volume ls -q)
```

```
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker volume rm $(docker volume ls -q)
```
